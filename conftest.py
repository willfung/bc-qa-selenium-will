import pytest
import allure
from allure_commons.types import AttachmentType

@pytest.fixture(scope='class')
def selenium_setup_and_teardown(request):
    from selenium import webdriver
    # web_driver = webdriver.Chrome(executable_path = 'drivers/chromedriver')
    # web_driver = webdriver.Firefox(executable_path = 'drivers/geckodriver')

    options = webdriver.ChromeOptions()
    capabilities = options.to_capabilities()
    web_driver = webdriver.Remote(desired_capabilities=capabilities, command_executor='http://localhost:4444/wd/hub')

    # web_driver.set_window_position(1441, -540)  # (3680, -540)
    request.cls.driver = web_driver
    yield
    web_driver.close()

@pytest.mark.hookwrapper
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    request = getattr(item, '_request', None)
    driver_defined = hasattr(request.cls, 'driver')
    if driver_defined:
        driver = request.cls.driver
    else:
        driver = None
    xfail = hasattr(report, 'wasxfail')
    failure = (report.skipped and xfail) or (report.failed and not xfail)
    when = item.config.getini('selenium_capture_debug').lower()
    capture_debug = when == 'always' or (when == 'failure' and failure)
    if capture_debug:
        exclude = item.config.getini('selenium_exclude_debug').lower()
        if driver is not None:
            if 'screenshot' not in exclude:
                allure.attach(driver.get_screenshot_as_png(), name = "Screenshot", attachment_type = AttachmentType.PNG)
