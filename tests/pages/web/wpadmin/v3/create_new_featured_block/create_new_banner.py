from selenium.webdriver.common.by import By
from pypom import Region
from selenium.common.exceptions import ElementNotSelectableException, NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select

from pages.web.wpadmin.v3.create_new_featured_block.new_banner_and_hero_slide_base import BannerHeroSlideBasePage

class CreateNewBannerPage(BannerHeroSlideBasePage):

    _page_heading_locator = (By.CSS_SELECTOR, "h1.wp-heading-inline")
    _name_of_banner_locator = (By.CSS_SELECTOR, "input[data-key='settings-banner-name']")
    _image_locator = (By.CSS_SELECTOR, "button#js--open-pick--bw_banner_image")
    _title_locator = (By.CSS_SELECTOR, "input#fm-bw_banner_content-0-bw_banner_title-0")
    _description_locator = (By.CSS_SELECTOR, "iframe#fm-bw_banner_content-0-bw_banner_description-0_ifr")
    _callout_text_locator = (By.CSS_SELECTOR, "input#fm-bw_banner_content-0-bw_banner_callout_text-0")
    _callout_url_locator = (By.CSS_SELECTOR, "input#fm-bw_banner_content-0-bw_banner_callout_url-0")

    #Banner Styles
    _select_colour_locator = (By.CSS_SELECTOR, "button.button.wp-color-result")
    _background_colour_input_locator = (By.CSS_SELECTOR, "input#fm-bw_banner_style-0-bw_banner_background_color-0")
    _image_position_locator = (By.CSS_SELECTOR, "select#fm-bw_banner_style-0-bw_banner_image_position-0")
    _title_colour_input_locator = (By.CSS_SELECTOR, "input#fm-bw_banner_style-0-bw_banner_title_color-0")
    _description_colour_input_locator = (By.CSS_SELECTOR, "input#fm-bw_banner_style-0-bw_banner_description_color-0")
    _callout_style_locator = (By.CSS_SELECTOR, "select#fm-bw_banner_style-0-bw_banner_callout_style-0")
    _link_text_colour_input_locator = (By.CSS_SELECTOR, "input#fm-bw_banner_style-0-bw_banner_link_text_color-0")

    #Taxonomies
    _audience_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_audience_category-0']")
    _audience_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_audience_category_0_chosen > div > ul > li")
    _related_format_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_related_format-0']")
    _related_format_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_related_format_0_chosen > div > ul > li")
    _programs_and_campaigns_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bc_program-0']")
    _programs_and_campaigns_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bc_program_0_chosen > div > ul > li")
    _genre_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_tax_genre-0']")
    _genre_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_tax_genre_0_chosen > div > ul > li")
    _topic_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-bw_tax_topic-0']")
    _topic_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_bw_tax_topic_0_chosen > div > ul > li")
    _tags_dropdown_locator = (By.CSS_SELECTOR, "div > div > label[for='fm-bw_taxonomy-0-post_tag-0']")
    _tags_dropdown_results_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_post_tag_0_chosen > div > ul > li")
    _tags_field_search_choices_locator = (By.CSS_SELECTOR, "#fm_bw_taxonomy_0_post_tag_0_chosen > ul > li.search-choice > span")

    @property
    def loaded(self):
        return self.find_element(*self._page_heading_locator)

    @property
    def name_of_banner(self):
        return self.find_element(*self._name_of_banner_locator)

    @property
    def image(self):
        return self.find_element(*self._image_locator)

    @property
    def title(self):
        return self.find_element(*self._title_locator)

    @property
    def description(self):
        return self.find_element(*self._description_locator)

    @property
    def callout_text(self):
        return self.find_element(*self._callout_text_locator)

    @property
    def callout_url(self):
        return self.find_element(*self._callout_url_locator)

    @property
    def background_colour(self):
        select_colour = self.find_elements(*self._select_colour_locator)
        return select_colour[0]

    @property
    def background_colour_input(self):
        return self.find_element(*self._background_colour_input_locator)

    @property
    def image_position(self):
        return self.find_element(*self._image_position_locator)

    def select_image_position(self, position):
        options = ["Bottom", "Middle", "Top"]
        select = Select(self.image_position)
        if position in options:
            select.select_by_visible_text(position)
        else:
            raise ElementNotSelectableException("Option not found.")

    @property
    def title_colour(self):
        select_colour = self.find_elements(*self._select_colour_locator)
        return select_colour[1]

    @property
    def description_colour(self):
        select_colour = self.find_elements(*self._select_colour_locator)
        return select_colour[2]

    @property
    def callout_style(self):
        return self.find_element(*self._callout_style_locator)

    def select_callout_style(self, style):
        options = ["Button", "Link"]
        select = Select(self.callout_style)
        if style in options:
            select.select_by_visible_text(style)
        else:
            raise ElementNotSelectableException("Option not found.")

    class CalloutStyleButton(Region):

        _select_colour_locator = (By.CSS_SELECTOR, "button.button.wp-color-result")
        _button_background_colour_input_locator = (By.CSS_SELECTOR, "input#fm-bw_banner_style-0-bw_banner_button_background_color-0")
        _button_text_colour_input_locator = (By.CSS_SELECTOR, "input#fm-bw_banner_style-0-bw_banner_button_text_color-0")

        @property
        def button_background_colour(self):
            select_colour = self.find_elements(*self._select_colour_locator)
            return select_colour[3]

        @property
        def button_background_colour_input(self):
            return self.find_element(*self._button_background_colour_input_locator)

        @property
        def button_text_colour(self):
            select_colour = self.find_elements(*self._select_colour_locator)
            return select_colour[4]

        @property
        def button_text_colour_input(self):
            return self.find_element(*self._button_text_colour_input_locator)

    class CalloutStyleLink(Region):

        _select_colour_locator = (By.CSS_SELECTOR, "button.button.wp-color-result")
        _link_text_colour_input_locator = (By.CSS_SELECTOR, "input#fm-bw_banner_style-0-bw_banner_link_text_color-0")

        @property
        def link_text_colour(self):
            select_colour = self.find_elements(*self._select_colour_locator)
            return select_colour[5]

        @property
        def link_text_colour_input(self):
            return self.find_element(*self._link_text_colour_input_locator)

    def taxonomy(self, taxonomy):
        taxonomies = {
            "audience": self._audience_dropdown_locator,
            "related format": self._related_format_dropdown_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_locator,
            "genre": self._genre_dropdown_locator,
            "topic": self._topic_dropdown_locator,
            "tags": self._tags_dropdown_locator
        }

        return self.find_element(*(taxonomies.get(taxonomy.casefold())))

    def select_taxonomy(self, taxonomy_dropdown, taxonomy_name):
        taxonomies_dropdowns_results = {
            "audience": self._audience_dropdown_results_locator,
            "related format": self._related_format_dropdown_results_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_results_locator,
            "genre": self._genre_dropdown_results_locator,
            "topic": self._topic_dropdown_results_locator,
            "tags": self._tags_dropdown_results_locator
        }

        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
        results = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))

        for index, result in enumerate(results):
            try:
                if result.get_attribute("textContent") == taxonomy_name:
                    self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    # Redefining temporary list to avoid StaleElementException
                    _ = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy_dropdown.casefold())))
                    return _[index]
            except Exception as exception:
                raise exception
        else:
            raise NoSuchElementException

    @property
    def is_tags_taxonomy_displayed(self):
        try:
            return self.find_element(*self._tags_dropdown_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_tags_taxonomy_term_selected(self):
        results = self.find_elements(*self._tags_field_search_choices_locator)

        if len(results) > 0:
            return True
        else:
            return False

    def taxonomy_terms(self, taxonomy):
        taxonomies_dropdowns_results = {
            "audience": self._audience_dropdown_results_locator,
            "related format": self._related_format_dropdown_results_locator,
            "programs and campaigns": self._programs_and_campaigns_dropdown_results_locator,
            "genre": self._genre_dropdown_results_locator,
            "topic": self._topic_dropdown_results_locator,
            "tags": self._tags_dropdown_results_locator
        }

        _ = []

        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        self.wait.until(EC.presence_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        self.wait.until(EC.visibility_of_all_elements_located(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        results = self.find_elements(*(taxonomies_dropdowns_results.get(taxonomy.casefold())))
        for index, result in enumerate(results):
            _.append(result.get_attribute("textContent"))

        return _
