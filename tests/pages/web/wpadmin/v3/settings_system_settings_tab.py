from selenium.webdriver.common.by import By

from .settings_base import SettingsBasePage

class SettingsSystemSettingsTabPage(SettingsBasePage):

    #System Settings Locators
    _library_brand_code_locator = (By.CSS_SELECTOR, "input[id='bc_brand_code']")
    _url_sub_domain_locator = (By.CSS_SELECTOR, "input[id='bc_slug']")
    _biblioevents_enabled_locator = (By.CSS_SELECTOR, "input[name='events_is_enabled'][value='1']")
    _biblioevents_disabled_locator = (By.CSS_SELECTOR, "input[name='events_is_enabled'][value='0']")
    _events_url_sub_domain_locator = (By.CSS_SELECTOR, "input[id='bc_events_slug']")
    _enabled_evanced_signup_locator = (By.CSS_SELECTOR, "input[name='evanced_is_enabled'][value='1']")
    _disabled_evanced_signup_locator = (By.CSS_SELECTOR, "input[name='evanced_is_enabled'][value='0']")
    _evanced_xml_feed_url_locator = (By.CSS_SELECTOR, "input[id='evanced_xml_feed']")
    _evanced_calendar_url_locator = (By.CSS_SELECTOR, "input[id='evanced_calendar_url']")
    _library_name_locator = (By.CSS_SELECTOR, "input[id='bc_lib_name']")
    _vanity_url_locator = (By.CSS_SELECTOR, "input[id='vanity_url']")
    _enabled_browse_module_locator = (By.CSS_SELECTOR, "input[name='browse_is_enabled'][value='1']")
    _disabled_browse_module_locator = (By.CSS_SELECTOR, "input[name='browse_is_enabled'][value='0']")
    _enabled_locations_module_locator = (By.CSS_SELECTOR, "input[name='locations_is_enabled'][value='1']")
    _disabled_locations_module_locator = (By.CSS_SELECTOR, "input[name='locations_is_enabled'][value='0']")
    _enabled_preferred_locations_module_locator = (By.CSS_SELECTOR, "input[name='preferred_locations_is_enabled'][value='1']")
    _disabled_preferred_locations_module_locator = (By.CSS_SELECTOR, "input[name='preferred_locations_is_enabled'][value='0']")
    _enabled_archives_module_locator = (By.CSS_SELECTOR, "input[name='archives_is_enabled'][value='1']")
    _disabled_archives_module_locator = (By.CSS_SELECTOR, "input[name='archives_is_enabled'][value='0']")
    _enabled_fictype_filter_locator = (By.CSS_SELECTOR, "input[name='library_has_fictype'][value='1']")
    _disabled_fictype_filter_locator = (By.CSS_SELECTOR, "input[name='library_has_fictype'][value='0']")
    _hide_audience_format_carousel_locator = (By.CSS_SELECTOR, "input[name='hide_audience_format_carousels'][value='1']")
    _show_audience_format_carousel_locator = (By.CSS_SELECTOR, "input[name='hide_audience_format_carousels'][value='2']")
    _always_use_new_hold_button_locator = (By.CSS_SELECTOR, "input[name='bw_new_hold_button'][value='1']")
    _use_environment_default_locator = (By.CSS_SELECTOR, "input[name='bw_new_hold_button'][value='0']")
    _show_staff_and_community_icons_yes_locator = (By.CSS_SELECTOR, "input[name='show_staff_icons'][value='1']")
    _show_staff_and_community_icons_no_locator = (By.CSS_SELECTOR, "input[name='show_staff_icons'][value='0']")
    _library_address_locator = (By.CSS_SELECTOR, "textarea[id='bc_lib_address']")
    _time_zone_code_locator = (By.CSS_SELECTOR, "input[id='bc_time_zone']")
    _google_webmaster_site_verification_locator = (By.CSS_SELECTOR, "input[id='site_verification']")
    _comments_on_news_hide_locator = (By.CSS_SELECTOR, "input[name='comments_on_news'][value='0']")
    _comments_on_news_show_locator = (By.CSS_SELECTOR, "input[name='comments_on_news'][value='1']")
    _comments_on_blogs_hide_locator = (By.CSS_SELECTOR, "input[name='comments_on_blogs'][value='0']")
    _comments_on_blogs_show_locator = (By.CSS_SELECTOR, "input[name='comments_on_blogs'][value='1']")
    _facebook_app_id_locator = (By.CSS_SELECTOR, "input[id='facebook_app_id']")
    _browse_audiobooks_hide_locator = (By.CSS_SELECTOR, "input[name='browse_audiobooks'][value='1']")
    _browse_audiobooks_show_locator = (By.CSS_SELECTOR, "input[name='browse_audiobooks'][value='2']")
    _html_editor_hide_locator = (By.CSS_SELECTOR, "input[name='bc_show_html_mode'][value='1']")
    _html_editor_show_locator = (By.CSS_SELECTOR, "input[name='bc_show_html_mode'][value='2']")
    _enabled_programs_enabled_locator = (By.CSS_SELECTOR, "input[name='bc_programs_are_enabled'][value='1']")
    _enabled_programs_disabled_locator = (By.CSS_SELECTOR, "input[name='bc_programs_are_enabled'][value='0']")
    _maintenance_page_disabled_locator = (By.CSS_SELECTOR, "input[name='bw_maintenance_mode'][value='0']")
    _maintenance_page_enabled_locator = (By.CSS_SELECTOR, "input[name='bw_maintenance_mode'][value='1']")
    _internal_vwo_script_disabled_locator = (By.CSS_SELECTOR, "input[name='bw_internal_vwo_script'][value='0']")
    _internal_vwo_script_enabled_locator = (By.CSS_SELECTOR, "input[name='bw_internal_vwo_script'][value='1']")
    _internal_crazy_egg_script_disabled_locator = (By.CSS_SELECTOR, "input[name='bw_internal_crazy_egg_script'][value='0']")
    _internal_crazy_egg_script_enabled_locator = (By.CSS_SELECTOR, "input[name='bw_internal_crazy_egg_script'][value='1']")

    #Donate Button Settings Locators
    _donate_button_disabled_locator = (By.CSS_SELECTOR, "input[name='donate_button_is_enable'][value='2']")
    _donate_button_homepage_locator = (By.CSS_SELECTOR, "input[name='donate_button_is_enable'][value='3']")
    _donate_button_interior_locator = (By.CSS_SELECTOR, "input[name='donate_button_is_enable'][value='4']")
    _donate_button_homepage_and_interior_locator = (By.CSS_SELECTOR, "input[name='donate_button_is_enable'][value='5']")
    _donate_button_url = (By.CSS_SELECTOR, "input[id='donate_button_url']")

    #Google Analytics Settings Locators
    _google_analytics_tracking_id_locator = (By.CSS_SELECTOR, "input[id='ga_id_stage']")
    _cross_domain_tracking_no_locator = (By.CSS_SELECTOR, "input[name='enable_cross_domain_tracking'][value='0']")
    _cross_domain_tracking_yes_locator = (By.CSS_SELECTOR, "input[name='enable_cross_domain_tracking'][value='1']")

    #Chat Widget Settings Locators
    _chat_widget_display_code_locator = (By.CSS_SELECTOR, "textarea[id='bc_chat_widget_display']")
    _chat_widget_footer_javascript_locator = (By.CSS_SELECTOR, "textarea[id='bc_chat_widget_display']")
    _show_chat_widget_hide_locator = (By.CSS_SELECTOR, "input[name='bc_chat_widget_display_in_header'][value='1']")
    _show_chat_widget_show_locator = (By.CSS_SELECTOR, "input[name='bc_chat_widget_display_in_header'][value='2']")

    #Google Translate Plugin Locator
    _widget_code_locator = (By.CSS_SELECTOR, "textarea[id='bc_google_translate_code']")

    #BiblioWeb V3 Settings Locators
    _v3_status_disabled_locator = (By.CSS_SELECTOR, "input[name='bw_v3_status'][value='1']")
    _v3_status_implementing_locator = (By.CSS_SELECTOR, "input[name='bw_v3_status'][value='2']")
    _v3_status_enabled_locator = (By.CSS_SELECTOR, "input[name='bw_v3_status'][value='3']")

    @property
    def loaded(self):
        return self.TabNavigation.system_settings

    @property
    def library_brand_code(self):
        return self.find_element(*self._library_brand_code_locator)

    @property
    def url_sub_domain(self):
        return self.find_element(*self._url_sub_domain_locator)

    @property
    def biblioevents_enabled_module(self):
        return self.find_element(*self._biblioevents_enabled_locator)

    @property
    def biblioevents_disabled_module(self):
        return self.find_element(*self._biblioevents_disabled_locator)

    @property
    def events_url_sub_domain(self):
        return self.find_element(*self._events_url_sub_domain_locator)

    @property
    def evanced_signup_enabled(self):
        return self.find_element(*self._enabled_evanced_signup_locator)

    @property
    def evanced_signup_disabled(self):
        return self.find_element(*self._disabled_evanced_signup_locator)

    @property
    def evanced_xml_feed_url(self):
        return self.find_element(*self._evanced_xml_feed_url_locator)

    @property
    def evanced_calendar_url(self):
        return self.find_element(*self._evanced_calendar_url_locator)

    @property
    def library_name(self):
        return self.find_element(*self._library_name_locator)

    @property
    def vanity_url(self):
        return self.find_element(*self._vanity_url_locator)

    @property
    def browse_enable_module(self):
        return self.find_element(*self._enabled_browse_module_locator)

    @property
    def browse_disable_module(self):
        return self.find_element(*self._disabled_browse_module_locator)

    @property
    def locations_enable_module(self):
        return self.find_element(*self._enabled_locations_module_locator)

    @property
    def locations_disable_module(self):
        return self.find_element(*self._disabled_locations_module_locator)

    @property
    def preferred_locations_enable_module(self):
        return self.find_element(*self._enabled_preferred_locations_module_locator)

    @property
    def preferred_locations_disable_module(self):
        return self.find_element(*self._disabled_preferred_locations_module_locator)

    @property
    def archives_enable_module(self):
        return self.find_element(*self._enabled_archives_module_locator)

    @property
    def archives_disable_module(self):
        return self.find_element(*self._disabled_archives_module_locator)

    @property
    def fictype_filter_enable(self):
        return self.find_element(*self._enabled_fictype_filter_locator)

    @property
    def fictype_filter_disable(self):
        return self.find_element(*self._disabled_fictype_filter_locator)

    @property
    def audience_format_carousel_hide(self):
        return self.find_element(*self._hide_audience_format_carousel_locator)

    @property
    def audience_format_carousel_show(self):
        return self.find_element(*self._show_audience_format_carousel_locator)

    @property
    def hold_button_use(self):
        return self.find_element(*self._always_use_new_hold_button_locator)

    @property
    def hold_button_environment_default(self):
        return self.find_element(*self._use_environment_default_locator)

    @property
    def staff_and_community_icons_on_cards_yes(self):
        return self.find_element(*self._show_staff_and_community_icons_yes_locator)

    @property
    def staff_and_community_icons_on_cards_no(self):
        return self.find_element(*self._show_staff_and_community_icons_no_locator)

    @property
    def library_address(self):
        return self.find_element(*self._library_address_locator)

    @property
    def time_zone_code(self):
        return self.find_element(*self._time_zone_code_locator)

    @property
    def google_webmaster_site_verification(self):
        return self.find_element(*self._google_webmaster_site_verification_locator)

    @property
    def comments_on_news_hide(self):
        return self.find_element(*self._comments_on_news_hide_locator)

    @property
    def comments_on_news_show(self):
        return self.find_element(*self._comments_on_news_show_locator)

    @property
    def comments_on_blogs_hide(self):
        return self.find_element(*self._comments_on_blogs_hide_locator)

    @property
    def comments_on_blogs_show(self):
        return self.find_element(*self._comments_on_blogs_show_locator)

    @property
    def facebook_app_id(self):
        return self.find_element(*self._facebook_app_id_locator)

    @property
    def browse_audiobooks_hide(self):
        return self.find_element(*self._browse_audiobooks_hide_locator)

    @property
    def browse_audiobooks_show(self):
        return self.find_element(*self._browse_audiobooks_show_locator)

    @property
    def html_editor_hide(self):
        return self.find_element(*self._html_editor_hide_locator)

    @property
    def html_editor_show(self):
        return self.find_element(*self._html_editor_show_locator)

    @property
    def enabled_programs_enabled(self):
        return self.find_element(*self._enabled_programs_enabled_locator)

    @property
    def enabled_programs_disabled(self):
        return self.find_element(*self._enabled_programs_disabled_locator)

    @property
    def maintenance_page_disabled(self):
        return self.find_element(*self._maintenance_page_disabled_locator)

    @property
    def maintenance_page_enabled(self):
        return self.find_element(*self._maintenance_page_enabled_locator)

    @property
    def internal_crazy_egg_script_disabled(self):
        return self.find_element(*self._internal_crazy_egg_script_disabled_locator)

    @property
    def internal_crazy_egg_script_enabled(self):
        return self.find_element(*self._internal_crazy_egg_script_enabled_locator)

    @property
    def donate_button_disabled(self):
        return self.find_element(*self._donate_button_disabled_locator)

    @property
    def donate_button_homepage(self):
        return self.find_element(*self._donate_button_homepage_locator)

    @property
    def donate_button_interior_pages(self):
        return self.find_element(*self._donate_button_interior_locator)

    @property
    def donate_button_homepage_and_interior_pages(self):
        return self.find_element(*self._donate_button_homepage_and_interior_locator)

    @property
    def donate_button_url(self):
        return self.find_element(*self._donate_button_url)

    @property
    def google_analytics_tracking_id(self):
        return self.find_element(*self._google_analytics_tracking_id_locator)

    @property
    def cross_domain_tracking_no(self):
        return self.find_element(*self._cross_domain_tracking_no_locator)

    @property
    def cross_domain_tracking_yes(self):
        return self.find_element(*self._cross_domain_tracking_yes_locator)

    @property
    def chat_widget_display_code(self):
        return self.find_element(*self._chat_widget_display_code_locator)

    @property
    def chat_widget_footer_javascript(self):
        return self.find_element(*self._chat_widget_footer_javascript_locator)

    @property
    def chat_widget_in_header_hide(self):
        return self.find_element(*self._show_chat_widget_hide_locator)

    @property
    def chat_widget_in_header_show(self):
        return self.find_element(*self._show_chat_widget_show_locator)

    @property
    def widget_code(self):
        return self.find_element(*self._widget_code_locator)

    @property
    def v3_status_disabled(self):
        return self.find_element(*self._v3_status_disabled_locator)

    @property
    def v3_status_implementing(self):
        return self.find_element(*self._v3_status_implementing_locator)

    @property
    def v3_status_enabled(self):
        return self.find_element(*self._v3_status_enabled_locator)
