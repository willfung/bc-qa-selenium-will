from selenium.webdriver.common.by import By
import json
from pypom import Region
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import ElementNotSelectableException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC

class ImageCropper(Region):

    _cropper_modal_header_locator = (By.CSS_SELECTOR, "h3[data-key='settings-crop-modal-title']")
    _cropper_locator = (By.CSS_SELECTOR, "#crop-container > div > div > div.cropper-crop-box")
    _cropper_box_locator = (By.CSS_SELECTOR, "#crop-container > div > div > div.cropper-drag-box.cropper-modal.cropper-crop")
    _crop_1_crop_2_locator = (By.CSS_SELECTOR, "div.media-frame-contents > div > h3")
    _crop_image_locator = (By.CSS_SELECTOR, "button[data-key='settings-crop-modal-crop-image']")
    _use_different_image_for_crop_1_and_crop_2_locator = (By.CSS_SELECTOR, "a[data-key='settings-crop-modal-different-image']")
    _cancel_locator = (By.CSS_SELECTOR, "button[data-key='settings-crop-modal-cancel']")
    _next_and_done_locator = (By.CSS_SELECTOR, "button[data-key='settings-crop-modal-next']")
    _previous_locator = (By.CSS_SELECTOR, "button[data-key='settings-crop-modal-previous']")

    @property
    def is_cropper_modal_visible(self):
        self.wait.until(EC.visibility_of_all_elements_located(self._cropper_modal_header_locator))
        return True

    @property
    def is_cropper_modal_not_visible(self):
        modal = self.find_elements(*self._cropper_modal_header_locator)
        self.wait.until((EC.invisibility_of_element_located(modal[1])))
        return True

    @property
    def is_cropper_box_visible(self):
        self.wait.until(EC.visibility_of_element_located(self._cropper_box_locator))
        return True

    @property
    def is_crop_one_visible(self):
        crop_one = self.find_element(*self._crop_1_crop_2_locator)
        if crop_one.get_attribute("textContent") == "Crop 1 (of 2)":
            return True
        else:
            return False

    @property
    def is_crop_two_visible(self):
        crop_one = self.find_element(*self._crop_1_crop_2_locator)
        if crop_one.get_attribute("textContent") == "Crop 2 (of 2)":
            return True
        else:
            return False

    def image_one_crop(self, width, height):
        self.wait.until(EC.visibility_of_element_located(self._cropper_locator))
        crop_box = self.find_element(*self._cropper_locator)
        self.driver.execute_script("arguments[0].style.cssText = 'width: {}; height: {};'".format(width, height), crop_box)

    def image_two_crop(self, width, height):
        self.wait.until(EC.visibility_of_element_located(self._cropper_locator))
        crop_box = self.find_element(*self._cropper_locator)
        self.driver.execute_script("arguments[0].style.cssText = 'width: {}; height: {};'".format(width, height), crop_box)

    @property
    def crop_image(self):
        self.wait.until(EC.element_to_be_clickable(self._crop_image_locator))
        return self.find_element(*self._crop_image_locator)

    @property
    def use_different_image_for_crop_1(self):
        return self.find_element(*self._use_different_image_for_crop_1_and_crop_2_locator)

    @property
    def use_different_image_for_crop_2(self):
        return self.find_element(*self._use_different_image_for_crop_1_and_crop_2_locator)

    @property
    def cancel(self):
        return self.find_element(*self._cancel_locator)

    @property
    def previous(self):
        return self.find_element(*self._previous_locator)

    @property
    def next(self):
        self.wait.until(EC.presence_of_element_located(self._next_and_done_locator))
        return self.find_element(*self._next_and_done_locator)

    @property
    def done(self):
        self.wait.until(EC.presence_of_element_located(self._next_and_done_locator))
        return self.find_element(*self._next_and_done_locator)
