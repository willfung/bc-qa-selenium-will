from pages.web.components.wpheader import WPHeader
from pages.web.components.wpsidemenu import WPSideMenu
from pages.core.base import BasePage

class StaffBasePage(BasePage):

    # If WordPress user logged in and navigated to a user-facing page,
    # user will have access to both BC Core header and WordPress.
    @property
    def wpheader(self):
        return WPHeader(self)

    @property
    def wpsidemenu(self):
        return WPSideMenu(self)