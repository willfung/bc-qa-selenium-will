from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from pages.core.base import BasePage
from pages.core.components.checked_out_item import CheckedOutItem


class CheckedOutPage(BasePage):
    URL_TEMPLATE = "/v2/checkedout/out"

    _heading_locator = (By.CSS_SELECTOR, ".cp-core-external-header")
    _checked_out_item_locator = (By.CSS_SELECTOR, ".cp-bib-list-item.cp-checked-out-item.out")
    _checked_out_list_locator = (By.CSS_SELECTOR, "div.cp-checked-out-list")
    _checked_out_list_empty_locator = (By.CSS_SELECTOR, "div.cp-empty-content-panel")
    _checked_out_notification_success_locator = (By.CSS_SELECTOR, ".alert.cp-alert.alert-success.cp-alert-success")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._heading_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def items(self):
        return [CheckedOutItem(self, element) for element in self.find_elements(*self._checked_out_item_locator)]

    @property
    def checked_out_list(self):
        return self.find_element(*self._checked_out_list_locator)

    @property
    def is_checked_out_list_displayed(self):
        try:
            return self.find_element(*self._checked_out_list_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def checked_out_list_empty(self):
        return self.find_element(*self._checked_out_list_empty_locator)

    @property
    def is_checked_out_list_empty_displayed(self):
        try:
            return self.find_element(*self._checked_out_list_empty_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def is_checked_out_success_notification_displayed(self):
        try:
            return self.find_element(*self._checked_out_notification_success_locator).is_displayed()
        except NoSuchElementException:
            return False
