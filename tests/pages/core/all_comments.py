from selenium.webdriver.common.by import By
from pypom import Region
from pages.core.bib import BibPage


class BibCommentPage(BibPage):
    #TODO Selectors
    _root_locator = (By.ID, "ugc_content_wrapper")
    _comment_locator = (By.CSS_SELECTOR, "div[id*='ugc_container_']")

    @property
    def loaded(self):
        return self.wait.untill(lambda w: self.find_element(*self._comment_locator).is_displayed())

    @property
    def comment_items(self):
        return [AllCommentsPageComment(self, element) for element in self.find_elements(*self._comment_locator)]


class AllCommentsPageComment(Region):

    _date_locator = (By.CLASS_NAME, "date")
    _username_locator = (By.CSS_SELECTOR, "[testid*='user_card_username']")
    _content_locator = (By.CLASS_NAME, "parts ")

    @property
    def date(self):
        return self.find_element(*self._date_locator)

    @property
    def username(self):
        return self.find_element(*self._username_locator)

    @property
    def content(self):
        return self.find_element(*self._content_locator)
