from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from pages.core.messages import MessagesPage

# https://<library>.<environment>.bibliocommons.com/messages/show/{message_id}
class ShowMessagePage(MessagesPage):

    URL_TEMPLATE = "/messages/show/{message_id}" # Usage: ShowMessagePage(self.driver, base_url, message_id = [MessageID]).open()

    _view_message_locator = (By.CSS_SELECTOR, "[data-test-id='view-message-tab']")
    _received_message_locator = (By.CLASS_NAME, "well")
    _top_message_container_locator = (By.CSS_SELECTOR, "[data-test-id='top-message-container']")

    @property
    def loaded(self):
        try:
            return self.find_element(*self._view_message_locator).is_displayed()
        except NoSuchElementException:
            return False

    @property
    def received_message(self):
        return self.find_element(*self._received_message_locator)
