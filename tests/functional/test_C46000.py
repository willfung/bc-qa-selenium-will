import pytest
import allure
import sure
import re
import sys
sys.path.append('tests')
from datetime import date, timedelta
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.bib import BibPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46000: Go to a UGC-heavy bib page, eg. Twilight")
@allure.testcase("", "TestRail")
class TestC46000:
    def test_C46000(self):
        home_page = HomePage(self.driver, "https://chipublib.demo.bibliocommons.com/v2").open()
        search_results_page = home_page.header.search_for("Booked")
        # Wait for search results to be greater than 0:
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.search_result_items[0].bib_title.click()
        bib_page = BibPage(self.driver)
        bib_page.wait.until(lambda s: bib_page.circulation.loaded)
        stats = bib_page.circulation.circulation_stats.text.split("\n")
        stats[0].should.match(r"Total Copies: \d+")
        stats[1].should.match(r"Available: \d+")
        stats[2].should.match(r"On Hold: \d+")
        # Get the number of total copies and convert it to an integer:
        total_copies = int(re.search("\d+", stats[0]).group(0))
        # Get the number of on hold copies and convert it to an integer:
        on_hold_copies = int(re.search("\d+", stats[2]).group(0))
        # On hold copies should not exceed 100 times * total copies:
        on_hold_copies.should.be.within(0, (total_copies * 100))
        # Ensure "Collection" is a string that is great than 0 characters:
        bib_page.circulation.collection_value.text.should.be.a(str)
        len(bib_page.circulation.collection_value.text).should.be.greater_than(0)
