import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system
import configuration.user
from pages.web.staff_base import StaffBasePage
from pages.web.wpadmin.login import LoginPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
# @allure.title("")
# @allure.testcase("", "TestRail")
class Test001:
    def test_001(self):

        staff_base_page = StaffBasePage(self.driver, configuration.system.base_url_web + "v3-page").open()
        staff_base_page.wpheader.is_wp_admin_header_displayed.should.be.false

        login_page = LoginPage(self.driver, configuration.system.base_url_web).open()
        login_page.log_in(configuration.user.name_lib_admin, configuration.user.pw_lib_admin)
        login_page.wait.until(lambda s: login_page.wpheader.is_wp_admin_header_displayed)

        staff_base_page = StaffBasePage(self.driver, configuration.system.base_url_web + "v3-page").open()
        staff_base_page.wpheader.is_wp_admin_header_displayed.should.be.true
        staff_base_page.wpheader.is_my_sites_menu_item_displayed.should.be.true
        staff_base_page.wpheader.is_my_site_name_menu_item_displayed.should.be.true
        staff_base_page.wpheader.is_customize_menu_item_displayed.should.be.true
        staff_base_page.wpheader.is_updates_menu_item_displayed.should.be.false
        staff_base_page.wpheader.is_my_unread_comments_menu_item_displayed.should.be.true
        staff_base_page.wpheader.is_add_new_content_menu_item_displayed.should.be.true
        staff_base_page.wpheader.is_edit_content_menu_item_displayed.should.be.true
        staff_base_page.wpheader.is_view_content_menu_item_displayed.should.be.false
        staff_base_page.wpheader.is_page_builder_menu_item_displayed.should.be.true
        staff_base_page.wpheader.is_my_account_menu_item_displayed.should.be.true

        staff_base_page.wpheader.my_sites.text.should.match("My Sites")
        staff_base_page.wpheader.my_site_name.text.should.match(r"[a-zA-Z ]*Public Library")
        staff_base_page.wpheader.customize.text.should.match("Customize")
        staff_base_page.wpheader.my_unread_comments_count.text.should.equal("0")
        staff_base_page.wpheader.add_new_content.text.should.match("New")
        staff_base_page.wpheader.edit_content.text.should.match("Edit Page")
        staff_base_page.wpheader.page_builder.text.should.match("Page Builder")
        staff_base_page.wpheader.page_builder_dot_colour.should.match("#6bc373")
        staff_base_page.wpheader.my_account_display_name.text.should.match(configuration.user.name_lib_admin)

        staff_base_page.wpheader.edit_content.click()
        staff_base_page.wpheader.is_view_content_menu_item_displayed.should.be.true
        staff_base_page.wpheader.view_content.text.should.match("View Page")