import pytest
import allure
import sure
import sys
sys.path.append('tests')
import configuration.system

from pages.core.home import HomePage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C47056: Empty Results")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C47056", "TestRail")
class TestC47056:
    def test_C47056(self):
        base_url = "https://chipublib.demo.bibliocommons.com/"
        homepage = HomePage(self.driver, base_url).open()
        search_results_page = homepage.header.search_for("the lorf of the rings")
        search_results_page.wait.until(lambda s: search_results_page.empty_search_result.is_displayed())
        search_results_page.empty_search_result.text.should.contain("Nothing found for ")
        search_results_page.did_you_mean_link.click()
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.search_result_items[0].bib_title.text.should.contain("Lord of the Rings")

        #for item in search_results_page.search_result_items:
        #    item.bib_title.text.should.contain("Lord")
