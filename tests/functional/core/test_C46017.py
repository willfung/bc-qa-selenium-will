import pytest
import allure
import sure
import sys
sys.path.append('tests')
from datetime import date, timedelta
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage

@pytest.mark.demo
@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46017: Search by Author or Subject")
@allure.testcase("https://bibliocommons.testrail.com/index.php?/cases/view/C46017", "TestRail")
class TestC46017:
    def test_C46017(self):
        home_page = HomePage(self.driver, "https://epl.demo.bibliocommons.com").open()
        home_page.header.log_in("21221011111111", "1234")
        if home_page.header.is_collapsible_search_trigger_displayed:
            home_page.header.collapsible_search_trigger.click()
        home_page.header.search_type.click()
        home_page.header.search_type_author.click()
        search_term = "Child, Lee"
        search_results_page = home_page.header.search_for(search_term) # , advanced_search = True)
        # Wait for search results to be greater than 0:
        search_results_page.wait.until(lambda s: (len(search_results_page.search_result_items) > 0))
        search_results_page.driver.current_url.should.match("&searchType=author$")
        for item in search_results_page.search_result_items:
            item.bib_author.text.should.equal(search_term)
