import pytest
import allure
import sure
import time
import sys
sys.path.append('tests')
from datetime import date, timedelta
import configuration.system

from pages.core.home import HomePage
from pages.core.v2.search_results import SearchResultsPage
from pages.core.bib import BibPage

@pytest.mark.usefixtures('selenium_setup_and_teardown')
@allure.title("C46001: Click all Community Activity tabs")
@allure.testcase("", "TestRail")
class TestC46001:
    def test_C46001(self):
        self.item_id = "2560434030"

        bib_page = BibPage(self.driver, configuration.system.base_url, item_id = self.item_id).open()
        time.sleep(5)
        comments = bib_page.community_activity_widget.comments
        print(len(comments))
        for comment in comments:
            if comment.is_ratings_displayed:
                print(comment.ratings)
            print(comment.date.text)
            print(comment.user.text)
            print(comment.content.text)
            print(150 * "-")
